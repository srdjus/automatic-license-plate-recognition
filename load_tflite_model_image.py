import numpy as np
import tensorflow as tf
import cv2
import os
import matplotlib
import matplotlib.pyplot as plt
from read_plate import read_plate

matplotlib.use("TkAgg")

# Source: https://www.tensorflow.org/lite/models/modify/model_maker/object_detection

# TODO: How to extract classes from model.tflite?


def preprocess_image(image_path, input_size):
    """Preprocess the input image to feed to the TFLite model"""
    img = tf.io.read_file(image_path)
    img = tf.io.decode_image(img, channels=3)
    img = tf.image.convert_image_dtype(img, tf.float32)
    original_image = img

    # Resizing image to CNN input_size
    resized_img = tf.image.resize(img, input_size)

    # Adding new axis to the front
    resized_img = resized_img[tf.newaxis, :]
    resized_img = tf.cast(resized_img, dtype=tf.float32)

    return resized_img, original_image


def detect_objects(interpreter: tf.lite.Interpreter, image, threshold):
    """Returns a list of detection results, each a dictionary of object info."""

    signature_fn = interpreter.get_signature_runner()

    # Feed the input image to the model
    output = signature_fn(input=image)

    # Get all outputs from the model
    count = int(np.squeeze(output["output_0"]))
    scores = np.squeeze(output["output_1"])
    classes = np.squeeze(output["output_2"])
    boxes = np.squeeze(output["output_3"])

    results = []
    for i in range(count):
        if scores[i] >= threshold:
            result = {
                "bounding_box": boxes[i],
                "class_id": classes[i],
                "score": scores[i],
            }
            results.append(result)
    return results


def run_odt_and_draw_results(image_path, interpreter, threshold=0.7):
    """Run object detection on the input image and draw the detection results"""
    # Load the input shape required by the model
    _, input_height, input_width, _ = interpreter.get_input_details()[0]["shape"]

    # Load the input image and preprocess it
    preprocessed_image, original_image = preprocess_image(
        image_path, (input_height, input_width)
    )

    # Run object detection on the input image
    results = detect_objects(interpreter, preprocessed_image, threshold=threshold)

    # Plot the detection results on the input image
    original_image_np = original_image.numpy()

    # Image with detections
    image_with_detections_np = np.copy(original_image_np)

    # Storing detected plates (as images)
    detected_plates = []

    for obj in results:
        # Convert the object bounding box from relative coordinates to absolute
        # coordinates based on the original image resolution
        ymin, xmin, ymax, xmax = obj["bounding_box"]
        xmin = int(xmin * original_image_np.shape[1])
        xmax = int(xmax * original_image_np.shape[1])
        ymin = int(ymin * original_image_np.shape[0])
        ymax = int(ymax * original_image_np.shape[0])

        detected_plates.append(original_image_np[ymin:ymax, xmin:xmax, :])

        # Draw the bounding box and label on the image
        cv2.rectangle(
            image_with_detections_np, (xmin, ymin), (xmax, ymax), (255, 0, 0), 2
        )

        # Make adjustments to make the label visible for all objects
        y = ymin - 15 if ymin - 15 > 15 else ymin + 15

        label = "{}: {:.0f}%".format("tablica", obj["score"] * 100)

        cv2.putText(
            image_with_detections_np,
            label,
            (xmin, y),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.5,
            (255, 0, 0),
            2,
        )

    # Return the final image
    return detected_plates, image_with_detections_np


saved_model_dir_path = os.path.abspath(
    "./workdir/models/my_ssd_mobnet/tflite/saved_model/"
)
tflite_model_path = os.path.join(saved_model_dir_path, "model.tflite")

# Loading tflite model
interpreter = tf.lite.Interpreter(model_path=tflite_model_path)
interpreter.allocate_tensors()

# Test image
image_example_path = os.path.abspath("./examples/bih.jpg")

# Running a recognition model and drawing boxes
detected_plates, image_with_detections_np = run_odt_and_draw_results(
    image_example_path, interpreter
)

valid_plates_count = 0
for i, plate in enumerate(detected_plates):
    ocr_result = read_plate(plate, logging=False)

    # Plate number cannot be parsed
    if ocr_result is None:
        continue

    valid_plates_count += 1
    ax = plt.subplot(len(detected_plates), 1, valid_plates_count)
    ax.axis("off")
    ax.set_title(ocr_result)
    plt.imshow(plate)

if valid_plates_count > 0:
    plt.show()

plt.imshow(image_with_detections_np)
plt.show()
