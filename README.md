## License plate (number) recognition
The project is split into two parts (logical entities).  First part is based on Tensorflow Object Detection API and its purpose is to detect license plate position on the image, after that (second part) license plate image is extracted from the original source and passed to OCR module in order to read actual numbers from the license plate. 
### License plate recognition
The model is trained on a custom dataset with approximately 600 hundred images (512 training/128 dev) on a [ssd_mobilenet_v2_fpnlite_640x640](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md) model. Dataset only includes Bosnian and Herzegovinian license plates. Images are manually annotated using [labelImg](https://github.com/heartexlabs/labelImg).
### Setting up environment
The first requirement is to have [Tensorflow Object Detection API](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/install.html) installed.

Also, in order to be able to use the model on low-performance hardware (e.g. w/o a graphics card), the same model can be adapted to use Tensorflow lite (examples at the end).
### Plate number recognition (OCR)
Considering this is the first version, OCR is done through [EasyOCR](https://github.com/JaidedAI/EasyOCR) package.
Specifically for BH plates, it might be better to train a new model using only adequate fonts, considering frequent mistakes in recognition of certain symbols. Likely to be implemented in the future.
### Running on a single image input
To test the model on a single image input (path to the image has to be specified):

`python load_model_image.py`

Similar for the lite version:

`python load_tflite_model_image.py`

![BH plate original](./assets/11.png "BH plate input")
![BH plate extraction](./assets/12.png "Plate extraction")
![BH plate output](./assets/13.png "BH plate output")

Non-BH plate:

![Non-BH plate output](./assets/21.png "Non-BH plate output")
![Non-BH plate extraction](./assets/22.png "Non-BH plate output")

### Running on a video input 
Same can be done in real-time using video capture and opencv (path to the video file or capture device has to be specified):

`python load_model.py`

Similar for the lite version:
`python load_tflite_model.py`
