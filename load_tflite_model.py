# Source: https://www.tensorflow.org/lite/models/modify/model_maker/object_detection
import numpy as np
import tensorflow as tf
import cv2
import os

from read_plate import read_plate


def preprocess_image(image_np, input_size):
    """Preprocess the input image to feed to the TFLite model"""
    img = tf.convert_to_tensor(image_np)
    img = tf.image.convert_image_dtype(img, tf.float32)

    # Resizing image to CNN input_size
    resized_img = tf.image.resize(img, input_size)

    # Adding new axis to the front
    resized_img = resized_img[tf.newaxis, :]
    resized_img = tf.cast(resized_img, dtype=tf.float32)

    return resized_img


def detect_objects(interpreter: tf.lite.Interpreter, image, threshold):
    """Returns a list of detection results, each a dictionary of object info."""
    signature_fn = interpreter.get_signature_runner()

    # Feed the input image to the model
    output = signature_fn(input=image)

    # Get all outputs from the model
    count = int(np.squeeze(output["output_0"]))
    scores = np.squeeze(output["output_1"])
    classes = np.squeeze(output["output_2"])
    boxes = np.squeeze(output["output_3"])

    results = []
    for i in range(count):
        if scores[i] >= threshold:
            result = {
                "bounding_box": boxes[i],
                "class_id": classes[i],
                "score": scores[i],
            }
            results.append(result)
    return results


def run_plate_detector(image_np, interpreter: tf.lite.Interpreter, threshold=0.7):
    """
    Tensorflow lite implementation of license plate detector
    Args:
    image_np -- numpy array/image to be processed,
    interpreter -- instance of interpreter to perform recognition task,
    threshold -- plate recognition treshold

    Returns:
    image_np_with_detections, plate_numbers -- a tuple, containing
        edited image and list of plate_numbers
    """
    # Load the input shape required by the model
    _, input_height, input_width, _ = interpreter.get_input_details()[0]["shape"]

    # Load the input image and preprocess it
    preprocessed_image = preprocess_image(image_np, (input_height, input_width))

    # Run object detection on the input image
    results = detect_objects(interpreter, preprocessed_image, threshold=threshold)

    # Plot the detection results on the input image
    image_np_with_detections = image_np.copy()

    # Cropped plate (numpy images)
    detected_plates = []

    for obj in results:
        # Convert the object bounding box from relative coordinates to absolute
        # coordinates based on the original image resolution
        ymin, xmin, ymax, xmax = obj["bounding_box"]
        xmin = int(xmin * image_np.shape[1])
        xmax = int(xmax * image_np.shape[1])
        ymin = int(ymin * image_np.shape[0])
        ymax = int(ymax * image_np.shape[0])

        # Draw the bounding box and label on the image
        cv2.rectangle(
            image_np_with_detections, (xmin, ymin), (xmax, ymax), (255, 0, 0), 2
        )

        # Make adjustments to make the label visible for all objects
        y = ymin - 15 if ymin - 15 > 15 else ymin + 15

        label = "tablica: {:.0f}%".format(obj["score"] * 100)

        cv2.putText(
            image_np_with_detections,
            label,
            (xmin, y),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.5,
            (255, 0, 0),
            2,
        )

        if xmin > 0 and ymin > 0:
            detected_plates.append(image_np[ymin:ymax, xmin:xmax, :])

    # License plate numbers as strings
    plate_numbers = []

    # Reading the number from the image
    for detected_plate in detected_plates:
        cv2.imshow(
            "Performing OCR on:", cv2.cvtColor(detected_plate, cv2.COLOR_BGR2RGB)
        )
        temp_output = read_plate(detected_plate)
        if temp_output:
            plate_numbers.append(temp_output)

    font = cv2.FONT_HERSHEY_COMPLEX

    # Write plate numbers onto the image
    for i, plate_number in enumerate(plate_numbers):
        cv2.putText(
            image_np_with_detections,
            plate_number,
            (50, 50 * (i + 1)),
            font,
            1,
            (255, 0, 0),
            2,
            cv2.LINE_AA,
        )

    return image_np_with_detections, plate_numbers


if __name__ == "__main__":
    # Usage example
    saved_model_dir_path = os.path.abspath(
        "./workdir/models/my_ssd_mobnet/tflite/saved_model/"
    )
    tflite_model_path = os.path.join(saved_model_dir_path, "model.tflite")

    # Loading tflite model
    interpreter = tf.lite.Interpreter(model_path=tflite_model_path)
    interpreter.allocate_tensors()

    video_path = os.path.abspath("./examples/video.mp4")

    cap = cv2.VideoCapture(video_path)  # Video capture
    # cap = cv2.VideoCapture(0) # Camera capture

    if not cap.isOpened():
        print("Cannot open the camera")
        exit()

    while True:
        ret, frame = cap.read()

        if not ret:
            print("Cannot receive frame (stream end?).")
            break

        image_np = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image_np_with_detections, plate_numbers = run_plate_detector(
            image_np, interpreter, 0.7
        )

        cv2.imshow("Capture", cv2.cvtColor(image_np_with_detections, cv2.COLOR_RGB2BGR))

        if cv2.waitKey(1) == ord("q"):
            break

    cap.release()
    cv2.destroyAllWindows()
