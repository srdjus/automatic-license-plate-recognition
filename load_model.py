import os
import tensorflow as tf
from object_detection.utils import (
    label_map_util,
    config_util,
    visualization_utils as viz_utils,
)
from object_detection.builders import model_builder
import cv2
import numpy as np

from read_plate import read_plate

pipeline_config_path = os.path.abspath("./workdir/models/my_ssd_mobnet/pipeline.config")

# Loading model from pipeline configuration file
configs = config_util.get_configs_from_pipeline_file(pipeline_config_path)
detection_model = model_builder.build(model_config=configs["model"], is_training=False)

# Restore checkpoint
ckpt = tf.compat.v2.train.Checkpoint(model=detection_model)

# Path to the checkpoint
ckpt.restore(os.path.abspath("./workdir/models/my_ssd_mobnet/ckpt-6")).expect_partial()

label_map_path = os.path.abspath("./workdir/annotations/label_map.pbtxt")
category_index = label_map_util.create_category_index_from_labelmap(label_map_path)


@tf.function
def detect_fn(image):
    image, shapes = detection_model.preprocess(image)
    prediction_dict = detection_model.predict(image, shapes)
    detections = detection_model.postprocess(prediction_dict, shapes)
    return detections


def run_plate_detector(image_np, threshold=0.7):
    """
    Tensorflow implementation of license plate detector
    Args:
    image_np -- numpy array/image to be processed,
    threshold -- plate recognition treshold

    Returns:
    image_np_with_detections, plate_numbers -- a tuple, containing
        edited image and list of plate_numbers
    """
    # Adding axis
    image_np_expanded = np.expand_dims(image_np, axis=0)

    # Convert numpy array to  tf tensor
    input_tensor = tf.convert_to_tensor(image_np_expanded, dtype=tf.float32)

    # Run plate detection model
    detections = detect_fn(input_tensor)

    # Number of detections (stored in an array field)
    num_detections = int(detections.pop("num_detections"))

    # Convert fields to numpy types
    detections = {
        key: value[0, :num_detections].numpy() for key, value in detections.items()
    }

    # Set fields
    detections["num_detections"] = num_detections
    detections["detection_classes"] = detections["detection_classes"].astype(np.int64)

    label_id_offset = 1

    # Drawing on a copy
    image_np_with_detections = image_np.copy()

    viz_utils.visualize_boxes_and_labels_on_image_array(
        image_np_with_detections,
        detections["detection_boxes"],
        detections["detection_classes"] + label_id_offset,
        detections["detection_scores"],
        category_index,
        use_normalized_coordinates=True,
        max_boxes_to_draw=5,
        min_score_thresh=threshold,
        agnostic_mode=False,
    )

    # Storing detected plates (as images)
    detected_plates = []

    # Looping through detections
    for i in range(num_detections):
        if detections["detection_scores"][i] > threshold:
            ymin, xmin, ymax, xmax = detections["detection_boxes"][i]

            # Calculate box coordinates (absolute)
            ymin = int(ymin * image_np.shape[0])
            ymax = int(ymax * image_np.shape[0])
            xmin = int(xmin * image_np.shape[1])
            xmax = int(xmax * image_np.shape[1])

            # Add plate image to the array
            detected_plates.append(image_np[ymin:ymax, xmin:xmax, :])

    plate_numbers = []

    # Running OCR
    for detected_plate in detected_plates:
        cv2.imshow(
            "Performing OCR on:", cv2.cvtColor(detected_plate, cv2.COLOR_RGB2BGR)
        )
        temp_output = read_plate(detected_plate)

        if temp_output:
            plate_numbers.append(temp_output)

    # Write plate numbers onto the image
    for i, plate_number in enumerate(plate_numbers):
        cv2.putText(
            image_np_with_detections,
            plate_number,
            (50, 50 * (i + 1)),
            cv2.FONT_HERSHEY_COMPLEX,
            1,
            (255, 0, 0),
            2,
            cv2.LINE_AA,
        )

    return image_np_with_detections, plate_numbers


if __name__ == "__main__":
    # Usage example
    video_path = os.path.abspath("./examples/video.mp4")

    cap = cv2.VideoCapture(video_path)  # Video capture
    # cap = cv2.VideoCapture(0) # Camera capture

    if not cap.isOpened():
        print("Cannot open the camera")
        exit()

    while True:
        ret, frame = cap.read()

        if not ret:
            print("Cannot receive frame (stream end?).")
            break

        # Convert BGR to RGB
        image_np = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        image_np_with_detections, plate_numbers = run_plate_detector(image_np, 0.7)

        # Switch back to BGR for OpenCV and show it
        cv2.imshow("Capture", cv2.cvtColor(image_np_with_detections, cv2.COLOR_RGB2BGR))

        if cv2.waitKey(1) == ord("q"):
            break

    cap.release()
    cv2.destroyAllWindows()
