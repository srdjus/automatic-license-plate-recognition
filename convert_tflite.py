import tensorflow as tf
import os

# TODO: Convert float32 to uint8 - https://www.tensorflow.org/lite/performance/model_optimization
saved_model_dir_path = os.path.abspath(
    "./workdir/models/my_ssd_mobnet/tflite/saved_model/"
)

# Convert the model
converter = tf.lite.TFLiteConverter.from_saved_model(saved_model_dir_path)
tflite_model = converter.convert()

tflite_model_file_path = os.path.join(saved_model_dir_path, "model.tflite")

# Save the model.
with open(tflite_model_file_path, "wb") as f:
    f.write(tflite_model)
