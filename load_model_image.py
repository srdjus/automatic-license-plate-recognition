import os
from charset_normalizer import detect
import tensorflow as tf
from object_detection.utils import (
    label_map_util,
    config_util,
    visualization_utils as viz_utils,
)

from object_detection.builders import model_builder
from PIL import Image
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from read_plate import read_plate

matplotlib.use("TkAgg")

pipeline_config_path = os.path.abspath("./workdir/models/my_ssd_mobnet/pipeline.config")

# Loading model from pipeline configuration file
configs = config_util.get_configs_from_pipeline_file(pipeline_config_path)
detection_model = model_builder.build(model_config=configs["model"], is_training=False)

# Restore checkpoint
ckpt = tf.compat.v2.train.Checkpoint(model=detection_model)

# Path to the checkpoint
ckpt.restore(os.path.abspath("./workdir/models/my_ssd_mobnet/ckpt-6")).expect_partial()


@tf.function
def detect_fn(image):
    image, shapes = detection_model.preprocess(image)
    prediction_dict = detection_model.predict(image, shapes)
    detections = detection_model.postprocess(prediction_dict, shapes)
    return detections


label_map_path = os.path.abspath("./workdir/annotations/label_map.pbtxt")

category_index = label_map_util.create_category_index_from_labelmap(label_map_path)

# NOTE: Path to test image
image_example_path = os.path.abspath("./examples/slo.jpg")

img = Image.open(image_example_path)
image_np = np.asarray(img)
plt.imshow(image_np)
plt.show()

input_tensor = tf.convert_to_tensor(np.expand_dims(image_np, 0), dtype=tf.float32)
detections = detect_fn(input_tensor)

# Number of detections (stored in an array field)
num_detections = int(detections.pop("num_detections"))

# Convert fields to numpy types
detections = {
    key: value[0, :num_detections].numpy() for key, value in detections.items()
}

# Set fields
detections["num_detections"] = num_detections
detections["detection_classes"] = detections["detection_classes"].astype(np.int64)

label_id_offset = 1

#  Creating a duplicate image (to draw onto)
image_np_with_detections = image_np.copy()

# Storing detected plates (as images)
detected_plates = []

# Looping through detections
for i in range(num_detections):
    if detections["detection_scores"][i] > 0.7:
        print("License plate detected")

        ymin, xmin, ymax, xmax = detections["detection_boxes"][i]

        # Calculate box coordinates (absolute) - Region of interest
        ymin = int(ymin * image_np.shape[0])
        ymax = int(ymax * image_np.shape[0])
        xmin = int(xmin * image_np.shape[1])
        xmax = int(xmax * image_np.shape[1])

        # Add plate image to the array
        detected_plates.append(image_np[ymin:ymax, xmin:xmax, :])


valid_plates_count = 0
for i, plate in enumerate(detected_plates):
    ocr_result = read_plate(plate, logging=False)

    # Plate number cannot be parsed
    if ocr_result is None:
        continue

    valid_plates_count += 1
    ax = plt.subplot(len(detected_plates), 1, valid_plates_count)
    ax.axis("off")
    ax.set_title(ocr_result)
    plt.imshow(plate)

if valid_plates_count > 0:
    plt.show()

# Drawing boxes onto the image
viz_utils.visualize_boxes_and_labels_on_image_array(
    image_np_with_detections,
    detections["detection_boxes"],
    detections["detection_classes"] + label_id_offset,
    detections["detection_scores"],
    category_index,
    use_normalized_coordinates=True,
    max_boxes_to_draw=5,
    min_score_thresh=0.7,
    agnostic_mode=False,
)

plt.imshow(image_np_with_detections)
plt.show()
